from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from hashlib import md5
import time
import jwt
from app import app, db, login


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True, unique=True)
    email = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(128))
    roles = db.relationship('Role', secondary='user_roles')
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    about_me = db.Column(db.String(512))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')
    logs = db.relationship('AuthLog', backref='user', lazy='dynamic')

    def __repr__(self):
        return f'<User id={self.id}, username={self.username}>'

    def set_log(self, log_name, *, ip, user_agent, session_id):
        action_type = ActionType.query.filter_by(name='log_name').first()

        if action_type is not None:
            log = AuthLog(user=self.id, action=action_type, ip=ip,
                          user_agent=user_agent,
                          session_id=session_id)
            db.session.add(log)
            db.session.commit()

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return f'https://gravatar.com/avatar/{digest}?d=identicon&s={size}'

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        followed = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
                followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def get_reset_password_token(self, expires_in=(60*10)):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time.time() + expires_in},
            md5(app.config['SECRET_KEY'].encode('utf-8')).hexdigest(),
            algorithm='HS512').decode('utf-8')

    @staticmethod
    def verify_reset_password(token):
        try:
            id = jwt.decode(token, md5(
                    app.config['SECRET_KEY'].encode('utf-8')).hexdigest(),
                algorithms=['HS512'])['reset_password']
        except Exception as e:
            return

        return User.query.get(id)


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)


class UserRoles(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(
        db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    role_id = db.Column(
        db.Integer(), db.ForeignKey('role.id', ondelete='CASCADE'))


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(300))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return f'Post id={self.id}, author={self.user_id}, body={self.body}'


class ActionType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    users = db.relationship('AuthLog', backref='action', lazy='dynamic')

    def __repr__(self):
        return f'<ActionType id={self.id}, name={self.name}>'


class AuthLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    action_id = db.Column(db.Integer, db.ForeignKey('action_type.id'))
    ip = db.Column(db.String(64))
    user_agent = db.Column(db.String(10000))
    session_id = db.Column(db.String(128))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
