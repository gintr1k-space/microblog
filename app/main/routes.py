from flask import render_template, flash, redirect, url_for, request, \
    session, g, current_app
from flask_login import current_user, login_required
from flask_babel import _, get_locale
from werkzeug.urls import url_parse
from datetime import datetime
from app.models import User, Post, ActionType, AuthLog
from app import db
from app.main import bp
from app.main.forms import EditProfileForm, PostForm
from app.linebreak import nl2br


@bp.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.locale = str(get_locale())


@bp.route('/index', methods=['GET', 'POST'])
@bp.route('/', methods=['GET', 'POST'])
def index():

    page = request.args.get('page', 1, type=int)

    posts = {}
    form = {}
    if current_user.is_authenticated:
        posts = current_user.followed_posts().paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)

        form = PostForm()
        if form.validate_on_submit():
            post = Post(body=form.post.data, author=current_user)
            db.session.add(post)
            db.session.commit()
            flash(_('Your post is LIVE now!'))
            return redirect(url_for('main.index'))
    else:
        user = User.query.filter_by(username='GinTR1k').first_or_404()
        posts = user.posts.order_by(Post.timestamp.desc()).paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)

    next_url = url_for('main.index', page=posts.next_num) \
        if posts.has_next else False
    prev_url = url_for('main.index', page=posts.prev_num) \
        if posts.has_prev else False
    return render_template('index.html', title=_('Home'), form=form,
                           active='home', posts=posts.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for(
        'main.user', username=user.username, page=posts.next_num) \
        if posts.has_next else False
    prev_url = url_for(
        'main.user', username=user.username, page=posts.prev_num) \
        if posts.has_prev else False
    return render_template('user.html', title=user.username, user=user,
                           posts=posts.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data

        db.session.commit()
        flash(_('Your changes have been saved.'))

        return redirect(url_for('main.user', username=current_user.username))

    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me

    return render_template('edit_profile.html', title=_('Edit Profile'),
                           form=form)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.index'))

    if user == current_user:
        flash(_('You can\'t follow yourself'))
        return redirect(url_for('main.user', username=username))

    current_user.follow(user)
    db.session.commit()
    flash(_('You are following %(username)s', username=user.username))
    return redirect(url_for('main.user', username=user.username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.user', username=current_user.username))

    if user == current_user:
        flash(_('You can\'t unfollow yourself'))
        return redirect(url_for('main.user', username=current_user.username))

    current_user.unfollow(user)
    db.session.commit()
    flash(_('You are not following %(username)s', username=user.username))
    return redirect(url_for('main.user', username=user.username))


@bp.route('/explore')
@login_required
def explore():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.explore', page=posts.next_num) \
        if posts.has_next else False
    prev_url = url_for('main.explore', page=posts.prev_num) \
        if posts.has_prev else False
    return render_template('index.html', title=_('Explore'),
                           active='explore', posts=posts.items,
                           next_url=next_url, prev_url=prev_url)
